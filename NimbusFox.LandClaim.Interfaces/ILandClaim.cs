﻿namespace NimbusFox.LandClaim.Interfaces {
    public interface ILandClaim {
        /// <summary>
        /// Adds a uid of a user into the guest
        /// </summary>
        /// <param name="ownerUid"></param>
        /// <param name="guestUid"></param>
        void AddGuest(string ownerUid, string guestUid);
        void RemoveGuest(string ownerUid, string guestUid);
    }
}