﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.LandClaim.Event.Classes.Records;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.LandClaim.Event.Commands {
    public class AdminCommands : ICommandBuilder {
        private static Dictionary<string, (Vector3I pos1, Vector3I pos2)> CreateSelections { get; } = new Dictionary<string, (Vector3I pos1, Vector3I pos2)>();

        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (bits.Length > 1) {
                switch (bits[1].ToLower()) {
                    case "create":
                        return CreateClaim(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "release":
                        return ReleaseClaim(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "delete":
                        return DeleteClaim(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "rename":
                        return Rename(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                }
            }

            return "nimbusfox.landclaim.event.command.lca.commands";
        }

        public string Kind => "lca";
        public string Usage => "nimbusfox.landclaim.event.command.lca.description";
        public bool Public => false;

        // lca create
        private string CreateClaim(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {

            responseParams = new object[] { };

            if (bits.Length >= 1) {
                switch (bits[0].ToLower()) {
                    case "pos1":
                        return CreateClaimPos1(connection, api, out responseParams);
                    case "pos2":
                        return CreateClaimPos2(connection, api, out responseParams);
                }
            }

            if (!CreateSelections.ContainsKey(connection.Credentials.Uid)) {
                return "nimbusfox.landclaim.event.error.noRegion";
            }

            var region = CreateSelections[connection.Credentials.Uid];

            if (region.pos1 == Vector3I.Zero || region.pos2 == Vector3I.Zero) {
                return "nimbusfox.landclaim.event.error.noRegion";
            }

            var insideClaim = false;

            Helpers.VectorLoop(region.pos1, region.pos2, (x, y, z) => {
                var location = new Vector3I(x, y, z);

                insideClaim |= LandClaimHook.Instance.Database
                                   .SearchRecords<ClaimRecord>(r => r.Region.IsInside(location)).Count > 0;
            });

            if (!insideClaim) {
                var record = LandClaimHook.Instance.Database.CreateRecord<ClaimRecord>(Guid.NewGuid());

                record.Region = new VectorSquareI(region.pos1, region.pos2);

                CreateSelections.Remove(connection.Credentials.Uid);
            }

            return insideClaim ? "nimbusfox.landclaim.event.error.regionConflict" : "nimbusfox.landclaim.event.success.createdRegion";
        }

        // lca create pos1
        private string CreateClaimPos1(ClientServerConnection connection, ICommandsApi api, out object[] responseParams) {
            responseParams = new object[] { };

            var entity = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (LandClaimHook.Instance.Database
                .SearchRecords<ClaimRecord>(x => x.Region.IsInside(entity.FeetLocation().From3Dto3I())).Any()) {
                return "nimbusfox.landclaim.event.error.regionConflict";
            }

            if (!CreateSelections.ContainsKey(connection.Credentials.Uid)) {
                CreateSelections.Add(connection.Credentials.Uid, (Vector3I.Zero, Vector3I.Zero));
            }

            var region = CreateSelections[connection.Credentials.Uid];

            CreateSelections[connection.Credentials.Uid] = (entity.FeetLocation().From3Dto3I(), region.pos2);
            responseParams = new object[] { entity.FeetLocation().X, entity.FeetLocation().Y, entity.FeetLocation().Z };
            return "nimbusfox.landclaim.event.success.positionMarked";
        }

        // lca create pos2
        private string CreateClaimPos2(ClientServerConnection connection, ICommandsApi api, out object[] responseParams) {
            responseParams = new object[] { };

            var entity = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

            if (LandClaimHook.Instance.Database
                .SearchRecords<ClaimRecord>(x => x.Region.IsInside(entity.FeetLocation().From3Dto3I())).Any()) {
                return "nimbusfox.landclaim.event.error.regionConflict";
            }

            if (!CreateSelections.ContainsKey(connection.Credentials.Uid)) {
                CreateSelections.Add(connection.Credentials.Uid, (Vector3I.Zero, Vector3I.Zero));
            }

            var region = CreateSelections[connection.Credentials.Uid];

            CreateSelections[connection.Credentials.Uid] = (region.pos1, entity.FeetLocation().From3Dto3I());
            responseParams = new object[] { entity.FeetLocation().X, entity.FeetLocation().Y, entity.FeetLocation().Z };
            return "nimbusfox.landclaim.event.success.positionMarked";
        }
        // lca create

        // lca release
        private string ReleaseClaim(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                area.Builders = new List<string>();
                area.OwnerUid = "";
                area.ClaimName = "";

                return "nimbusfox.landclaim.event.success.regionUnclaimed";
            }

            return "";
        }
        // lca release

        // lca delete
        private string DeleteClaim(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                LandClaimHook.Instance.Database.RemoveRecord(area.ID);

                return "nimbusfox.landclaim.event.success.regionDeleted";
            }

            return "";
        }
        // lca delete

        // lca rename
        private string Rename(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                area.ClaimName = bits[0];
                responseParams = new object[] { area.ClaimName };
                return "nimbusfox.landclaim.event.success.renamedRegion";
            }

            return "";
        }
        // lca rename
    }
}
