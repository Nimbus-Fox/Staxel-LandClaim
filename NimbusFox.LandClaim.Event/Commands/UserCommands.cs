﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.LandClaim.Event.Classes.Records;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.LandClaim.Event.Commands {
    public class UserCommands : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (bits.Length > 1) {
                switch (bits[1].ToLower()) {
                    case "info":
                        return Info(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "claim":
                        return Claim(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "unclaim":
                        return UnClaim(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "rename":
                        return Rename(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "invite":
                        return InviteBuilder(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    case "kick":
                        return KickBuilder(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                }
            }

            return "nimbusfox.landclaim.event.command.lc.commands";
        }

        public string Kind => "lc";
        public string Usage => "nimbusfox.landlcaim.event.command.lc.description";
        public bool Public => true;

        // lc info
        private string Info(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };

            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                LandClaimHook.Instance.FxCore.MessagePlayerByEntity(player,
                    "nimbusfox.landclaim.event.message.regionInfo.1", area.ClaimName);
                LandClaimHook.Instance.FxCore.MessagePlayerByEntity(player,
                    "nimbusfox.landclaim.event.message.regionInfo.2",
                    LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(area.OwnerUid));
                LandClaimHook.Instance.FxCore.MessagePlayerByEntity(player,
                    "nimbusfox.landclaim.event.message.regionInfo.3",
                    string.Join(", ",
                        area.Builders.Select(LandClaimHook.Instance.FxCore.UserManager.GetNameByUid)));
                LandClaimHook.Instance.FxCore.MessagePlayerByEntity(player,
                    "nimbusfox.landclaim.event.message.regionInfo.4", area.Region.Start.X, area.Region.Start.Z,
                    area.Region.End.X, area.Region.End.Z);

            }

            return "";
        }
        // lc info

        // lc claim
        private string Claim(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                var claimCount = LandClaimHook.Instance.Database
                    .SearchRecords<ClaimRecord>(x => x.OwnerUid == player.PlayerEntityLogic.Uid()).Count;

                if (claimCount >= LandClaimHook.Instance.Config.ClaimLimit) {
                    responseParams = new object[] { claimCount.ToString(), LandClaimHook.Instance.Config.ClaimLimit.ToString() };
                    return "nimbusfox.landclaim.event.error.tooManyClaims";
                }

                if (!area.OwnerUid.IsNullOrEmpty()) {
                    responseParams = new object[] { LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(area.OwnerUid) };
                    return "nimbusfox.landclaim.event.error.regionClaimed";
                }

                area.OwnerUid = player.PlayerEntityLogic.Uid();
                area.Builders = new List<string>();

                return "nimbusfox.landclaim.event.success.regionClaimed";

            }

            return "";
        }
        // lc claim

        // lc unclaim
        private string UnClaim(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                if (area.OwnerUid == player.PlayerEntityLogic.Uid()) {
                    area.OwnerUid = "";
                    area.Builders = new List<string>();
                    area.ClaimName = "";

                    return "nimbusfox.landclaim.event.success.regionUnclaimed";
                }

                responseParams = new object[] { LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(area.OwnerUid) };
                return "nimbusfox.landclaim.event.error.doesNotOwnRegion";
            }

            return "";
        }
        // lc unclaim

        // lc rename
        private string Rename(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                if (area.OwnerUid == player.PlayerEntityLogic.Uid()) {
                    area.ClaimName = bits[0];
                    responseParams = new object[] { area.ClaimName };
                    return "nimbusfox.landclaim.event.success.renamedRegion";
                }

                responseParams = new object[] { LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(area.OwnerUid) };
                return "nimbusfox.landclaim.event.error.doesNotOwnRegion";
            }

            return "";
        }
        // lc rename

        // lc invite
        private string InviteBuilder(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                if (area.OwnerUid == player.PlayerEntityLogic.Uid()) {
                    var targetUid = LandClaimHook.Instance.FxCore.UserManager.GetUidByName(bits[0]);
                    if (targetUid.IsNullOrEmpty()) {
                        responseParams = new object[] {bits[0]};
                        return "nimbusfox.landclaim.event.error.invalidUser";
                    }

                    var builders = area.Builders;

                    if (builders.Contains(targetUid)) {
                        responseParams = new object[] {bits[0]};
                        return "nimbusfox.landclaim.event.error.alreadyBuilder";
                    }

                    builders.Add(targetUid);
                    area.Builders = builders;

                    responseParams = new object[] {LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(targetUid)};
                    return "nimbusfox.landclaim.event.success.newBuilder";
                }

                responseParams = new object[] { LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(area.OwnerUid) };
                return "nimbusfox.landclaim.event.error.doesNotOwnRegion";
            }

            return "";
        }
        // lc invite

        // lc kick
        private string KickBuilder(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[] { };
            if (LandClaimHook.Instance.FxCore.UserManager.IsUserOnline(connection.Credentials.Uid)) {
                var player = LandClaimHook.Instance.FxCore.UserManager.GetPlayerEntityByUid(connection.Credentials.Uid);

                var area = LandClaimHook.Instance.Database.SearchRecords<ClaimRecord>(x =>
                    x.Region.IsInside(player.Physics.Position)).FirstOrDefault();

                if (area == default(ClaimRecord)) {
                    return "nimbusfox.landclaim.event.message.noRegion";
                }

                if (area.OwnerUid == player.PlayerEntityLogic.Uid()) {
                    var targetUid = LandClaimHook.Instance.FxCore.UserManager.GetUidByName(bits[0]);
                    if (targetUid.IsNullOrEmpty()) {
                        responseParams = new object[] { bits[0] };
                        return "nimbusfox.landclaim.event.error.invalidUser";
                    }

                    var builders = area.Builders;

                    if (!builders.Contains(targetUid)) {
                        return "";
                    }

                    builders.Remove(targetUid);
                    area.Builders = builders;

                    responseParams = new object[] { LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(targetUid) };
                    return "nimbusfox.landclaim.event.success.kickBuilder";
                }

                responseParams = new object[] { LandClaimHook.Instance.FxCore.UserManager.GetNameByUid(area.OwnerUid) };
                return "nimbusfox.landclaim.event.error.doesNotOwnRegion";
            }

            return "";
        }
        // lc kick
    }
}
