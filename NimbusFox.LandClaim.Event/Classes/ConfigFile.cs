﻿using System.IO;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;

namespace NimbusFox.LandClaim.Event.Classes {
    public class ConfigFile : BlobFile {
        public ConfigFile(FileStream stream, bool binary = false) : base(stream, binary) {
            Defaults();
            ForceSave();
        }

        public long ClaimLimit {
            get => Blob.GetLong("claimLimit", 1);
            set => Blob.SetLong("claimLimit", value);
        }

        protected void Defaults() {
            ClaimLimit = ClaimLimit;
        }
    }
}
