﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.FoxCore.Classes;
using Plukit.Base;

namespace NimbusFox.LandClaim.Event.Classes.Records {
    public class ClaimRecord : BaseRecord {
        private BlobDatabase _database { get; }

        public ClaimRecord(BlobDatabase database, Blob blob, Guid id) : base(database, blob, id) {
            _database = database;
        }

        public string OwnerUid {
            get => _blob.GetString(nameof(OwnerUid), "");
            set {
                _blob.SetString(nameof(OwnerUid), value);
                Save();
            }
        }

        public VectorSquareI Region {
            get {
                if (!_blob.Contains(nameof(Region))) {
                    return null;
                }

                var region = _blob.FetchBlob(nameof(Region));

                if (!region.Contains("start") || !region.Contains("end")) {
                    return null;
                }

                return new VectorSquareI(region.FetchBlob("start").GetVector3I(), region.FetchBlob("end").GetVector3I());
            }
            set {
                var region = _blob.FetchBlob(nameof(Region));

                region.FetchBlob("start").SetVector3I(value.Start);
                region.FetchBlob("end").SetVector3I(value.End);
                Save();
            }
        }

        public List<string> Builders {
            get {
                if (!_blob.Contains(nameof(Builders))) {
                    return new List<string>();
                }

                if (_blob.KeyValueIteratable[nameof(Builders)].Kind != BlobEntryKind.List) {
                    return new List<string>();
                }

                return _blob.GetStringList(nameof(Builders)).ToList();
            }
            set {
                if (_blob.Contains(nameof(Builders))) {
                    _blob.Delete(nameof(Builders));
                }

                _blob.PrepareStringList(nameof(Builders), value.Count);

                for (var i = 0; i < value.Count; i++) {
                    _blob.SetStringList(nameof(Builders), i, value[i]);
                }
                Save();
            }
        }

        public string ClaimName {
            get => _blob.GetString(nameof(ClaimName), "");
            set {
                _blob.SetString(nameof(ClaimName), value);
                Save();
            }
        }
    }
}
