﻿using System.IO;
using System.Linq;
using NimbusFox.FoxCore;
using NimbusFox.FoxCore.Classes;
using NimbusFox.LandClaim.Event.Classes;
using NimbusFox.LandClaim.Event.Classes.Records;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;
using Staxel.Tiles;

namespace NimbusFox.LandClaim.Event {
    public class LandClaimHook : IFoxModHookV3 {

        public static LandClaimHook Instance { get; private set; }

        public Fox_Core FxCore { get; }

        public BlobDatabase Database { get; private set; }

        public ConfigFile Config { get; private set; }

        public LandClaimHook() {
            Instance = this;

            FxCore = new Fox_Core("nimbusfox", "BasicLandClaim", "Event");
        }

        public void Dispose() { }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }
        public void GameContextInitializeAfter() { }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            if (Database == null && universe.Server) {
                Database = new BlobDatabase(FxCore.SaveDirectory.ObtainFileStream("LandClaim.db", FileMode.OpenOrCreate), FxCore.LogError);

                Config = new ConfigFile(FxCore.ConfigDirectory.ObtainFileStream("Config.json", FileMode.OpenOrCreate));
            }
        }
        public void UniverseUpdateAfter() { }
        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            if (Database != null) {
                if (entity?.Logic is PlayerEntityLogic logic) {
                    if (logic.IsAdmin()) {
                        return true;
                    }

                    var records = Database.SearchRecords<ClaimRecord>(x => x.Region.IsInside(location));

                    return records.Any(x => x.Builders.Contains(logic.Uid()) || x.OwnerUid == logic.Uid());
                }
            }
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            if (Database != null) {
                if (entity?.Logic is PlayerEntityLogic logic) {
                    if (logic.IsAdmin()) {
                        return true;
                    }

                    var records = Database.SearchRecords<ClaimRecord>(x => x.Region.IsInside(location));

                    return records.Any(x => x.Builders.Contains(logic.Uid()) || x.OwnerUid == logic.Uid());
                }
            }
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            if (Database != null) {
                if (entity?.Logic is PlayerEntityLogic logic) {
                    if (logic.IsAdmin()) {
                        return true;
                    }

                    var records = Database.SearchRecords<ClaimRecord>(x => x.Region.IsInside(location));

                    return records.Any(x => x.Builders.Contains(logic.Uid()) || x.OwnerUid == logic.Uid());
                }
            }
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() { }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }

        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            if (Database != null) {
                if (entity?.Logic is PlayerEntityLogic logic) {
                    if (logic.IsAdmin()) {
                        return true;
                    }

                    var records = Database.SearchRecords<ClaimRecord>(x => x.Region.IsInside(location.ToVector3D()));

                    return records.Any(x => x.Builders.Contains(logic.Uid()) || x.OwnerUid == logic.Uid());
                }
            }
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            if (Database != null) {
                if (entity?.Logic is PlayerEntityLogic logic) {
                    if (logic.IsAdmin()) {
                        return true;
                    }

                    var records = Database.SearchRecords<ClaimRecord>(x => x.Region.IsInside(lookingAtEntity.Physics.Position));

                    return records.Any(x => x.Builders.Contains(logic.Uid()) || x.OwnerUid == logic.Uid());
                }
            }
            return true;
        }

        public void OnPlayerLoadAfter(Blob blob) { }
        public void OnPlayerSaveBefore(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerSaveAfter(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerConnect(Entity entity) { }
        public void OnPlayerDisconnect(Entity entity) { }
    }
}
