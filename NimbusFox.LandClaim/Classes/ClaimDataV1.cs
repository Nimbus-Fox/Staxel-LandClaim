﻿using System;
using System.Collections.Generic;

namespace NimbusFox.LandClaim.Classes {
    [Serializable]
    public class ClaimDataV1 {
        public List<ClaimAreaV1> ClaimedAreas { get; set; }

        public ClaimDataV1() {
            ClaimedAreas = new List<ClaimAreaV1>();
        }

        public List<ClaimAreaV1> CloneClaimedAreas() {
            return new List<ClaimAreaV1>(ClaimedAreas);
        }
    }
}
